## Angular + NestJS

## Installation

```bash
$ npm install
```

## Running the Angular app

```bash
# From Root.
$ cd client/image-cropper-client 

# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Running the Nest app

```bash
# From Root.
$ cd packages/image-cropper-server 

# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test Apps

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
