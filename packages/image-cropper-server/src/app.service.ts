import { Injectable } from '@nestjs/common';
import * as fs from 'fs';
import * as uuid from 'uuidv4';

@Injectable()
export class AppService {

  saveFilesLocally(file) {

    fs.writeFile(`${FILE_PATH}/${uuid()}${file.originalname}`, file.buffer, 'binary', (err) => {
      if (err) {
        return err;
      }
      return 'done';
  });
  }
}

export const FILE_PATH = '/tmp';
