import { Controller, Get, Post, UseInterceptors, UploadedFile, BadRequestException } from '@nestjs/common';
import { AppService } from './app.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { multerAvatarConnection } from './multer-avatar.connection';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello() {
    return { message : 'API endpoint please make a request to your required API' };
  }

  @Post('images/v1/saveAll')
  @UseInterceptors(FileInterceptor('file', multerAvatarConnection))
  saveFilesLocally(
    @UploadedFile('file') file,
  ) {
    return this.appService.saveFilesLocally(file);
  }
}
