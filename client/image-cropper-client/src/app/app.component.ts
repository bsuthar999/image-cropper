import { Component, OnInit, ViewChild } from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { IMAGE_TYPE, FAILED_TO_SAVE, CLOSE, SERVER_CHECK, SUCCESS, INVALID_FILE_DIMENSION } from './constants/policy-constants';
import { FormGroup, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild('stepper') stepper;
  imageChangedEvent: any = '';
  imageType: string;
  firstFormGroup: FormGroup;
  imageName1 = '';
  imageName2 = '';
  imageName3 = '';
  imageName4 = '';
  image = false;
  croppedImage: any = '';
  croppedImage1: any = '';
  croppedImage2: any = '';
  croppedImage3: any = '';

  constructor(
    private snackBar: MatSnackBar,
    private appService: AppService,
  ) {}

  ngOnInit() {
    this.firstFormGroup = new FormGroup({
      croppedImage: new FormControl(this.croppedImage)
    });
  }

  async fileChangeEvent(event: any) {
    const file = event.target.files[0];
    const fileType = file.type;
    // The data URL

    await this.validateSize(event);

    if (fileType && IMAGE_TYPE.includes(fileType)) {
      this.imageType = fileType;
      this.imageChangedEvent = event;
      this.image = true;
    } else {
      this.image = false;
      this.snackBar.open('Invalid File Type', CLOSE, { duration: 5500});
    }
  }

  imageCropped(event: ImageCroppedEvent) {
    this.imageName1 = '_750_X_450';
    this.croppedImage = event.base64;
  }
  imageCropped1(event: ImageCroppedEvent) {
    this.imageName2 = '_365_x_450';
    this.croppedImage1 = event.base64;
  }
  imageCropped2(event: ImageCroppedEvent) {
    this.imageName3 = '_365_x_212';
    this.croppedImage2 = event.base64;
  }
  imageCropped3(event: ImageCroppedEvent) {
    this.imageName4 = '_380_X_380';
    this.croppedImage3 = event.base64;
  }

  saveAllImages() {
    this.sendImage(
    this.croppedImage,
    this.imageType,
    this.imageName1).subscribe({
      next : response1 => {
        this.sendImage(
          this.croppedImage1,
          this.imageType,
          this.imageName2).subscribe({
            next: response2 => {
              this.sendImage(
                this.croppedImage2,
                this.imageType,
                this.imageName3).subscribe({
                  next: response3 => {
                    this.sendImage(
                      this.croppedImage3,
                      this.imageType,
                      this.imageName4).subscribe({
                        next : response4 => {
                          this.snackBar.open(SUCCESS, CLOSE, { duration: 5500});
                          this.resetStepper();
                        },
                        error : err => {
                          this.snackBar.open(`${FAILED_TO_SAVE}${this.imageName1}${SERVER_CHECK}`, CLOSE, { duration : 2500});
                        }
                      });
                  },
                  error : err => {
                    this.snackBar.open(`${FAILED_TO_SAVE}${this.imageName2}${SERVER_CHECK}`, CLOSE, { duration : 2500});
                  }
                });
            },
            error : err => {
              this.snackBar.open(`${FAILED_TO_SAVE}${this.imageName3}${SERVER_CHECK}`, CLOSE, { duration : 2500});
            }
          });
      },
      error : err => {
        this.snackBar.open(`${FAILED_TO_SAVE}${this.imageName4}${SERVER_CHECK}`, CLOSE, { duration : 2500});
      }
    });
  }

  sendImage(
    croppedImage,
    imageType,
    imageName
  ) {
    return this.appService.saveAllImages(
      croppedImage,
      imageType,
      imageName
      );
  }

  validateSize(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];

      const img = new Image();
      img.src = window.URL.createObjectURL( file );

      reader.readAsDataURL(file);
      reader.onload = () => {

        if (img.naturalWidth !== 1024 || img.naturalHeight !== 1024) {
          this.snackBar.open(INVALID_FILE_DIMENSION, CLOSE, { duration : 5500});
          this.resetStepper();
        }
    };
   }
  }

  resetStepper() {
    this.stepper.selectedIndex = 0;
    this.croppedImage = '';
    this.croppedImage1 = '';
    this.croppedImage2 = '';
    this.croppedImage3 = '';
    this.image = false;

  }

}
