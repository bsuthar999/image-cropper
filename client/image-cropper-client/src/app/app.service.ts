import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(
    private http: HttpClient
  ) { }

  serverURL = 'http://image.localhost:5111';

  saveAllImages(
    imageBuffer,
    imageType,
    imageName
  ) {

    const blob = this.uriToBlob(imageBuffer);
    const file = new File([blob], `${imageName}`, {
      type: imageType
    });

    const uploadData = new FormData();
    uploadData.append('file', file, file.name);

    return this.http.post(this.serverURL + '/images/v1/saveAll' ,  uploadData );
  }

  getAllImages() {
    return this.http.get(this.serverURL + 'images/v1/getAll');
  }


  uriToBlob(dataURI) {

    // convert base64/URLEncoded data component to raw binary data held in a string
    let byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0) {
    byteString = atob(dataURI.split(',')[1]);
    } else {
    byteString = unescape(dataURI.split(',')[1]);
    }
    // separate out the mime component
    const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
    // write the bytes of the string to a typed array
    const ia = new Uint8Array(byteString.length);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ia], {type: mimeString});
  }
}
