
export const IMAGE_TYPE = [
    'image/peg',
    'image/jpeg',
    'image/exif',
    'image/tiff',
    'image/gif',
    'image/bmp',
    'image/png',
];
export const CLOSE = 'Close';
export const FAILED_TO_SAVE = 'failed to save image';
export const SERVER_CHECK = ' please make sure server is running';
export const SUCCESS = 'Images Saved locally please check the /tmp folder in your root.';
export const INVALID_FILE_DIMENSION = 'Invalid file size please select an image of size greater then 1024 X 1024';

